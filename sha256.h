#ifndef sha256_h_
#define sha256_h_

#include <iostream>
#include <string>

struct Chunk {
	//512-bit SHA256 block
	uint32_t word_15;
	uint32_t word_14;
	uint32_t word_13;
	uint32_t word_12;
	uint32_t word_11;
	uint32_t word_10;
	uint32_t word_9;
	uint32_t word_8;
	uint32_t word_7;
	uint32_t word_6;
	uint32_t word_5;
	uint32_t word_4;
	uint32_t word_3;
	uint32_t word_2;
	uint32_t word_1;
	uint32_t word_0;
};

static const uint32_t IV[64] = {	0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
									0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
							   		0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
							   		0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
							   		0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
							   		0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
							   		0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
							   		0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2	};

class sha256{
public:
	sha256();
	std::string getSHA256(uint8_t* byteStream, uint32_t size); 
	std::string getSHA256(std::string& text);
	std::string getDoubleSHA256(uint8_t* byteStream, uint32_t size);
	std::string getDoubleSHA256(std::string& text);
private:
	uint32_t rotr(uint32_t v, int32_t shift);
	//Initial hash component values
	uint32_t h0;
	uint32_t h1;
	uint32_t h2;
	uint32_t h3;
	uint32_t h4;
	uint32_t h5;
	uint32_t h6;
	uint32_t h7;

	uint32_t a;
	uint32_t b;
	uint32_t c;
	uint32_t d;
	uint32_t e;
	uint32_t f;
	uint32_t g;
	uint32_t h;
};

uint32_t sha256::rotr(uint32_t v, int32_t shift) {
	//Circular right bitshift
	//From: https://stackoverflow.com/questions/25799215/bitwise-rotation-circular-shift
    uint32_t s =  shift>=0? shift%32 : -((-shift)%32);
    return (v>>s) | (v<<(32-s));
}


sha256::sha256() {
	h0 = 0x6a09e667;
	h1 = 0xbb67ae85;
	h2 = 0x3c6ef372;
	h3 = 0xa54ff53a;
	h4 = 0x510e527f;
	h5 = 0x9b05688c;
	h6 = 0x1f83d9ab;
	h7 = 0x5be0cd19;	
}

std::string sha256::getSHA256(uint8_t* byteStream, uint32_t size) {
	int message_bits = size*8;
	message_bits += 72;
	int pad = 512 - (message_bits % 512);
	int message_bytes = (message_bits + pad)/8;
	uint8_t byte_message[message_bytes];
	memset(byte_message, 0, sizeof byte_message);
	for(int i = 0; i < size; ++i) {
		byte_message[i] = byteStream[i];
	}
	byte_message[size] = 0x80; 

	uint64_t message_length_pad = (uint64_t)(size*8);
	uint8_t length_byte_0 = (uint8_t)((message_length_pad & 0xFF00000000000000) >> 56);
	uint8_t length_byte_1 = (uint8_t)((message_length_pad & 0x00FF000000000000) >> 48);
	uint8_t length_byte_2 = (uint8_t)((message_length_pad & 0x0000FF0000000000) >> 40);
	uint8_t length_byte_3 = (uint8_t)((message_length_pad & 0x000000FF00000000) >> 32);
	uint8_t length_byte_4 = (uint8_t)((message_length_pad & 0x00000000FF000000) >> 24);
	uint8_t length_byte_5 = (uint8_t)((message_length_pad & 0x0000000000FF0000) >> 16);
	uint8_t length_byte_6 = (uint8_t)((message_length_pad & 0x000000000000FF00) >> 8);
	uint8_t length_byte_7 = (uint8_t)((message_length_pad & 0x00000000000000FF));

	byte_message[message_bytes - 8] = length_byte_0;
	byte_message[message_bytes - 7] = length_byte_1;
	byte_message[message_bytes - 6] = length_byte_2;
	byte_message[message_bytes - 5] = length_byte_3;
	byte_message[message_bytes - 4] = length_byte_4;
	byte_message[message_bytes - 3] = length_byte_5;
	byte_message[message_bytes - 2] = length_byte_6;
	byte_message[message_bytes - 1] = length_byte_7;

	int numChunks = message_bytes / 64;
	Chunk messageC[numChunks];
	int pos = 0;
	for(int i = 0; i < numChunks; ++i) {
		uint32_t word = 0x00000000;
		word |= (uint32_t)(byte_message[pos] << 24);
		word |= (uint32_t)(byte_message[pos + 1] << 16);
		word |= (uint32_t)(byte_message[pos + 2] << 8);
		word |= (uint32_t)(byte_message[pos + 3]);
		messageC[i].word_15 = word;

		word = 0x00000000;
		word |= (uint32_t)(byte_message[pos + 4] << 24);
		word |= (uint32_t)(byte_message[pos + 5] << 16);
		word |= (uint32_t)(byte_message[pos + 6] << 8);
		word |= (uint32_t)(byte_message[pos + 7]);
		messageC[i].word_14 = word;

		word = 0x00000000;
		word |= (uint32_t)(byte_message[pos + 8] << 24);
		word |= (uint32_t)(byte_message[pos + 9] << 16);
		word |= (uint32_t)(byte_message[pos + 10] << 8);
		word |= (uint32_t)(byte_message[pos + 11]);
		messageC[i].word_13 = word;

		word = 0x00000000;
		word |= (uint32_t)(byte_message[pos + 12] << 24);
		word |= (uint32_t)(byte_message[pos + 13] << 16);
		word |= (uint32_t)(byte_message[pos + 14] << 8);
		word |= (uint32_t)(byte_message[pos + 15]);
		messageC[i].word_12 = word;

		word = 0x00000000;
		word |= (uint32_t)(byte_message[pos + 16] << 24);
		word |= (uint32_t)(byte_message[pos + 17] << 16);
		word |= (uint32_t)(byte_message[pos + 18] << 8);
		word |= (uint32_t)(byte_message[pos + 19]);
		messageC[i].word_11 = word;

		word = 0x00000000;
		word |= (uint32_t)(byte_message[pos + 20] << 24);
		word |= (uint32_t)(byte_message[pos + 21] << 16);
		word |= (uint32_t)(byte_message[pos + 22] << 8);
		word |= (uint32_t)(byte_message[pos + 23]);
		messageC[i].word_10 = word;

		word = 0x00000000;
		word |= (uint32_t)(byte_message[pos + 24] << 24);
		word |= (uint32_t)(byte_message[pos + 25] << 16);
		word |= (uint32_t)(byte_message[pos + 26] << 8);
		word |= (uint32_t)(byte_message[pos + 27]);
		messageC[i].word_9 = word;

		word = 0x00000000;
		word |= (uint32_t)(byte_message[pos + 28] << 24);
		word |= (uint32_t)(byte_message[pos + 29] << 16);
		word |= (uint32_t)(byte_message[pos + 30] << 8);
		word |= (uint32_t)(byte_message[pos + 31]);
		messageC[i].word_8 = word;

		word = 0x00000000;
		word |= (uint32_t)(byte_message[pos + 32] << 24);
		word |= (uint32_t)(byte_message[pos + 33] << 16);
		word |= (uint32_t)(byte_message[pos + 34] << 8);
		word |= (uint32_t)(byte_message[pos + 35]);
		messageC[i].word_7 = word;

		word = 0x00000000;
		word |= (uint32_t)(byte_message[pos + 36] << 24);
		word |= (uint32_t)(byte_message[pos + 37] << 16);
		word |= (uint32_t)(byte_message[pos + 38] << 8);
		word |= (uint32_t)(byte_message[pos + 39]);
		messageC[i].word_6 = word;

		word = 0x00000000;
		word |= (uint32_t)(byte_message[pos + 40] << 24);
		word |= (uint32_t)(byte_message[pos + 41] << 16);
		word |= (uint32_t)(byte_message[pos + 42] << 8);
		word |= (uint32_t)(byte_message[pos + 43]);
		messageC[i].word_5 = word;

		word = 0x00000000;
		word |= (uint32_t)(byte_message[pos + 44] << 24);
		word |= (uint32_t)(byte_message[pos + 45] << 16);
		word |= (uint32_t)(byte_message[pos + 46] << 8);
		word |= (uint32_t)(byte_message[pos + 47]);
		messageC[i].word_4 = word;

		word = 0x00000000;
		word |= (uint32_t)(byte_message[pos + 48] << 24);
		word |= (uint32_t)(byte_message[pos + 49] << 16);
		word |= (uint32_t)(byte_message[pos + 50] << 8);
		word |= (uint32_t)(byte_message[pos + 51]);
		messageC[i].word_3 = word;

		word = 0x00000000;
		word |= (uint32_t)(byte_message[pos + 52] << 24);
		word |= (uint32_t)(byte_message[pos + 53] << 16);
		word |= (uint32_t)(byte_message[pos + 54] << 8);
		word |= (uint32_t)(byte_message[pos + 55]);
		messageC[i].word_2 = word;

		word = 0x00000000;
		word |= (uint32_t)(byte_message[pos + 56] << 24);
		word |= (uint32_t)(byte_message[pos + 57] << 16);
		word |= (uint32_t)(byte_message[pos + 58] << 8);
		word |= (uint32_t)(byte_message[pos + 59]);
		messageC[i].word_1 = word;

		word = 0x00000000;
		word |= (uint32_t)(byte_message[pos + 60] << 24);
		word |= (uint32_t)(byte_message[pos + 61] << 16);
		word |= (uint32_t)(byte_message[pos + 62] << 8);
		word |= (uint32_t)(byte_message[pos + 63]);
		messageC[i].word_0 = word;
		pos += 64;
	}

	uint32_t schedule[64] = {0};

	for(int i = 0; i < numChunks; ++i) {
		schedule[0] = messageC[i].word_15;
		schedule[1] = messageC[i].word_14;
		schedule[2] = messageC[i].word_13;
		schedule[3] = messageC[i].word_12;
		schedule[4] = messageC[i].word_11;
		schedule[5] = messageC[i].word_10;
		schedule[6] = messageC[i].word_9;
		schedule[7] = messageC[i].word_8;
		schedule[8] = messageC[i].word_7;
		schedule[9] = messageC[i].word_6;
		schedule[10] = messageC[i].word_5;
		schedule[11] = messageC[i].word_4;
		schedule[12] = messageC[i].word_3;
		schedule[13] = messageC[i].word_2;
		schedule[14] = messageC[i].word_1;
		schedule[15] = messageC[i].word_0;

		for(int j = 16; j < 64; ++j) {
			uint32_t s0 = (rotr(schedule[j - 15], 7))^(rotr(schedule[j - 15], 18))^(schedule[j - 15] >> 3);
			uint32_t s1 = (rotr(schedule[j - 2], 17))^(rotr(schedule[j - 2], 19))^(schedule[j - 2] >> 10);
			schedule[j] = schedule[j - 16] + s0 + schedule[j - 7] + s1;
		}

		a = h0;
		b = h1;
		c = h2;
		d = h3;
		e = h4;
		f = h5;
		g = h6;
		h = h7;

		for(int j = 0; j < 64; ++j) {
			uint32_t s1 = (rotr(e, 6))^(rotr(e, 11))^(rotr(e, 25));
			uint32_t ch = (e & f)^((~e) & g);
			uint32_t temp1 = h + s1 + ch + IV[j] + schedule[j];
			uint32_t s0 = (rotr(a, 2))^(rotr(a, 13))^(rotr(a, 22));
			uint32_t maj = (a & b)^(a & c)^(b & c);
			uint32_t temp2 = s0 + maj;
			h = g;
			g = f;
			f = e;
			e = d + temp1;
			d = c;
			c = b;
			b = a;
			a = temp1 + temp2;
		}
		h0 = h0 + a;
		h1 = h1 + b;
		h2 = h2 + c;
		h3 = h3 + d;
		h4 = h4 + e;
		h5 = h5 + f;
		h6 = h6 + g;
		h7 = h7 + h;
	}
	char hash[64];
	std::snprintf(hash, sizeof(hash) + 1, "%08x%08x%08x%08x%08x%08x%08x%08x", h0, h1, h2, h3, h4, h5, h6, h7);
	h0 = 0x6a09e667;
	h1 = 0xbb67ae85;
	h2 = 0x3c6ef372;
	h3 = 0xa54ff53a;
	h4 = 0x510e527f;
	h5 = 0x9b05688c;
	h6 = 0x1f83d9ab;
	h7 = 0x5be0cd19;
	return std::string(hash);
}

std::string sha256::getSHA256(std::string& text) {
	const char* cstr = text.c_str();
	uint8_t byteStream[text.size()];
	for(int i = 0; i < text.size(); ++i) {
		byteStream[i] = (uint8_t)cstr[i];
	}
	return sha256::getSHA256(byteStream, text.size());
}

#endif