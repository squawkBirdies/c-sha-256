#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>

using namespace std;

#include <openssl/sha.h>
#include "sha256.h"

uint32_t rand_old = 0;

void gen_random(char *s, const int len) {
    srand(time(NULL) + rand_old);
    rand_old = rand();
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    s[len] = 0;
}

string sha256(const string str)
{
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, str.c_str(), str.size());
    SHA256_Final(hash, &sha256);
    stringstream ss;
    for(int i = 0; i < SHA256_DIGEST_LENGTH; i++)
    {
        ss << hex << setw(2) << setfill('0') << (int)hash[i];
    }
    return ss.str();
}

int main() {
    sha256::sha256 a;
    char random[1000];
    bool failed = false;
    int iteration_count = 0;
    for(int i = 0; i < 5000000; ++i) {
        iteration_count = i;
        gen_random(random, 1000);
        string ran = string(random);
        string openssl_out = sha256(ran);
        string my_out = a.getSHA256(ran);
        if(openssl_out.compare(my_out) != 0) {
            cout<<"Failed for: "<<ran<<endl;
            failed = true;
            break;
        }
    }
    if(!failed) {
        cout<<"Test cases succeeded"<<endl;
        cout<<"Iterations: "<<iteration_count<<endl;
    }
    return 0;
}
