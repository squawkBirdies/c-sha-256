#include <iostream>
#include <string>

#include "sha256.h"

int main() {
	sha256 a;
	std::string message;
	std::cout << "Enter Message: " << std::endl;
	std::getline(std::cin, message);
	std::string round1 = a.getSHA256(message);
	std::cout<<"SHA256: 0x"<<round1<<std::endl;
	std::cout<<"Double SHA256: 0x"<<a.getSHA256(round1)<<std::endl;
}