#include <iostream>
#include <string>
#include <sstream>
#include <thread>
#include <chrono>
#include <iomanip>
#include <vector>
#include <time.h>

#include "sha256.h"

struct Block {
	std::string name;
	std::string payload;
	std::string previous_block_hash;
	uint64_t difficulty;
	uint64_t nonce;
	uint64_t block_number;
};

volatile bool nonce_found;
volatile bool miner_1_find;
volatile bool miner_2_find;
volatile bool miner_3_find;
volatile bool miner_4_find;

volatile int miner_1_duration = 0;
volatile int miner_2_duration = 0;
volatile int miner_3_duration = 0;
volatile int miner_4_duration = 0;

Block b;
Block b2;
Block b3;
Block b4;

std::vector<float> hashrate_avg;

std::string blockToString(Block& b) {
	std::string out = "";
	out += b.name;
	out += '\n';
	out += b.payload;
	out += '\n';
	out += b.previous_block_hash;
	out += '\n';
	out += std::to_string(b.difficulty);
	out += '\n';
	out += std::to_string(b.nonce);
	out += '\n';
	out += std::to_string(b.block_number);
	return out;
}

Block stringToBlock(std::string& s){
	std::stringstream ss(s.c_str());
	std::string line = "";
	Block out;
	std::getline(ss, line, '\n');
	out.name = line;
	std::getline(ss, line, '\n');
	out.payload = line;
	std::getline(ss, line, '\n');
	out.previous_block_hash = line;
	std::getline(ss, line, '\n');
	out.difficulty = (uint64_t)stoi(line);
	std::getline(ss, line, '\n');
	out.nonce = (uint64_t)stoi(line);
	std::getline(ss, line, '\n');
	out.block_number = (uint64_t)stoi(line);
	return out;
}

void mine1() {
	sha256 a;
	std::string block_serialized;
	std::string round_1;
	std::string round_2;
	while(!nonce_found) {
		std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
		block_serialized = blockToString(b);
		round_1 = a.getSHA256(block_serialized);
		round_2 = a.getSHA256(round_1);
		bool valid = true;
		for(int i = 0; i < b.difficulty; ++i) {
			if(round_2[i] != '0') {
				valid = false;
				break;
			}
		}
		if(valid) {
			nonce_found = true;
			miner_1_find = true;
		} else {
			b.nonce += 1;
		}
		std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
		miner_1_duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	}
}

void mine2() {
	sha256 d;
	std::string block_serialized;
	std::string round_1;
	std::string round_2;
	while(!nonce_found) {
		std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
		block_serialized = blockToString(b2);
		round_1 = d.getSHA256(block_serialized);
		round_2 = d.getSHA256(round_1);
		bool valid = true;
		for(int i = 0; i < b2.difficulty; ++i) {
			if(round_2[i] != '0') {
				valid = false;
				break;
			}
		}
		if(valid) {
			nonce_found = true;
			miner_2_find = true;
		} else {
			b2.nonce += 1;
		}
		std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
		miner_2_duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	}
}

void mine3() {
	sha256 a;
	std::string block_serialized;
	std::string round_1;
	std::string round_2;
	while(!nonce_found) {
		std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
		block_serialized = blockToString(b3);
		round_1 = a.getSHA256(block_serialized);
		round_2 = a.getSHA256(round_1);
		bool valid = true;
		for(int i = 0; i < b3.difficulty; ++i) {
			if(round_2[i] != '0') {
				valid = false;
				break;
			}
		}
		if(valid) {
			nonce_found = true;
			miner_3_find = true;
		} else {
			b3.nonce += 1;
		}
		std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
		miner_3_duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	}
}

void mine4() {
	sha256 d;
	std::string block_serialized;
	std::string round_1;
	std::string round_2;
	while(!nonce_found) {
		std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
		block_serialized = blockToString(b4);
		round_1 = d.getSHA256(block_serialized);
		round_2 = d.getSHA256(round_1);
		bool valid = true;
		for(int i = 0; i < b4.difficulty; ++i) {
			if(round_2[i] != '0') {
				valid = false;
				break;
			}
		}
		if(valid) {
			nonce_found = true;
			miner_4_find = true;
		} else {
			b4.nonce += 1;
		}
		std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
		miner_4_duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
	}
}

void performance_timer() {
	float miner_1_performance = 0.0;
	float miner_2_performance = 0.0;
	float miner_3_performance = 0.0;
	float miner_4_performance = 0.0;

	while(!nonce_found) {
		if(miner_1_duration != 0) {
			float seconds = ((float)miner_1_duration)/1000000.0;
			miner_1_performance = 1.0/seconds;
			miner_1_performance /= 1000.0;
		}
		std::cout<<"Miner 1: "<<std::setprecision(6)<<miner_1_performance<<" KH/s\t";

		if(miner_2_duration != 0) {
			float seconds = ((float)miner_2_duration)/1000000.0;
			miner_2_performance = 1.0/seconds;
			miner_2_performance /= 1000.0;
		}
		std::cout<<"Miner 2: "<<std::setprecision(6)<<miner_2_performance<<" KH/s\t";

		if(miner_3_duration != 0) {
			float seconds = ((float)miner_3_duration)/1000000.0;
			miner_3_performance = 1.0/seconds;
			miner_3_performance /= 1000.0;
		}
		std::cout<<"Miner 3: "<<std::setprecision(6)<<miner_3_performance<<" KH/s\t";

		if(miner_4_duration != 0) {
			float seconds = ((float)miner_4_duration)/1000000.0;
			miner_4_performance = 1.0/seconds;
			miner_4_performance /= 1000.0;
		}
		std::cout<<"Miner 4: "<<std::setprecision(6)<<miner_4_performance<<" KH/s\t";
		float total_hashrate = miner_1_performance + miner_2_performance + miner_3_performance + miner_4_performance;
		std::cout<<"Total: "<<std::setprecision(6)<<total_hashrate<<" KH/s"<<std::endl;
		hashrate_avg.push_back(total_hashrate);
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
}

int main() {
	sha256 c;
	b.name = "BaldEagleCoin";
	b.payload = "Maxwell -> Lawrence 25 BEC";
	b.previous_block_hash = "0x00000b67f1a047fdc5d3c4f2f74e97c85d59d909080131cdbe096985debbd47c";
	b.nonce = 0;
	b.block_number = 2;

	b2.name = b.name;
	b2.payload = b.payload;
	b2.previous_block_hash = b.previous_block_hash;
	b2.block_number = b.block_number;
	srand(time(NULL));
	b2.nonce = rand();

	b3.name = b.name;
	b3.payload = b.payload;
	b3.previous_block_hash = b.previous_block_hash;
	b3.block_number = b.block_number;
	srand(time(NULL) + b2.nonce);
	b3.nonce = rand();

	b4.name = b.name;
	b4.payload = b.payload;
	b4.previous_block_hash = b.previous_block_hash;
	b4.block_number = b.block_number;
	srand(time(NULL) + b3.nonce);
	b4.nonce = rand();

	nonce_found = false;
	int difficulty = 0;

	std::cout<<"Enter Difficulty: "<<std::endl;
	std::cin >> difficulty;

	b.difficulty = difficulty;
	b2.difficulty = difficulty;
	b3.difficulty = difficulty;
	b4.difficulty = difficulty;

	miner_1_find = false;
	miner_2_find = false;
	miner_3_find = false;
	miner_4_find = false;
	std::thread miner1(&mine1);
	std::thread miner2(&mine2);
	std::thread miner3(&mine3);
	std::thread miner4(&mine4);
	std::thread perform(&performance_timer);
	miner1.join();
	miner2.join();
	miner3.join();
	miner4.join();
	perform.join();

	float sum = 0.0;
	for(int i = 0; i < hashrate_avg.size(); ++i) {
		sum += hashrate_avg[i];
	}
	float avg = sum/((float)hashrate_avg.size());
	std::cout<<"\nAverage Hashrate: "<<std::setprecision(6)<<avg<<" KH/s"<<std::endl;

	if(miner_1_find) {
		std::cout<<"\nBlock Found by Miner 1!"<<std::endl;
		std::cout<<"Name: "<<b.name<<std::endl;
		std::cout<<"Payload: "<<b.payload<<std::endl;
		std::cout<<"Previous Block Hash: "<<b.previous_block_hash<<std::endl;
		std::cout<<"Nonce: "<<b.nonce<<std::endl;
		std::cout<<"Block Number: "<<b.block_number<<std::endl;
		std::cout<<"Difficulty: "<<b.difficulty<<std::endl;

		std::string block_serialized;
		std::string round_1;
		std::string round_2;
		block_serialized = blockToString(b);
		round_1 = c.getSHA256(block_serialized);
		round_2 = c.getSHA256(round_1);
		std::cout<<"Hash: 0x"<<round_2<<std::endl;
	} else if (miner_2_find) {
		std::cout<<"\nBlock Found by Miner 2!"<<std::endl;
		std::cout<<"Name: "<<b2.name<<std::endl;
		std::cout<<"Payload: "<<b2.payload<<std::endl;
		std::cout<<"Previous Block Hash: "<<b2.previous_block_hash<<std::endl;
		std::cout<<"Nonce: "<<b2.nonce<<std::endl;
		std::cout<<"Block Number: "<<b2.block_number<<std::endl;
		std::cout<<"Difficulty: "<<b2.difficulty<<std::endl;

		std::string block_serialized;
		std::string round_1;
		std::string round_2;
		block_serialized = blockToString(b2);
		round_1 = c.getSHA256(block_serialized);
		round_2 = c.getSHA256(round_1);
		std::cout<<"Hash: 0x"<<round_2<<std::endl;
	} else if(miner_3_find) {
		std::cout<<"\nBlock Found by Miner 3!"<<std::endl;
		std::cout<<"Name: "<<b3.name<<std::endl;
		std::cout<<"Payload: "<<b3.payload<<std::endl;
		std::cout<<"Previous Block Hash: "<<b3.previous_block_hash<<std::endl;
		std::cout<<"Nonce: "<<b3.nonce<<std::endl;
		std::cout<<"Block Number: "<<b3.block_number<<std::endl;
		std::cout<<"Difficulty: "<<b3.difficulty<<std::endl;

		std::string block_serialized;
		std::string round_1;
		std::string round_2;
		block_serialized = blockToString(b3);
		round_1 = c.getSHA256(block_serialized);
		round_2 = c.getSHA256(round_1);
		std::cout<<"Hash: 0x"<<round_2<<std::endl;
	} else if (miner_4_find) {
		std::cout<<"\nBlock Found by Miner 4!"<<std::endl;
		std::cout<<"Name: "<<b4.name<<std::endl;
		std::cout<<"Payload: "<<b4.payload<<std::endl;
		std::cout<<"Previous Block Hash: "<<b4.previous_block_hash<<std::endl;
		std::cout<<"Nonce: "<<b4.nonce<<std::endl;
		std::cout<<"Block Number: "<<b4.block_number<<std::endl;
		std::cout<<"Difficulty: "<<b4.difficulty<<std::endl;

		std::string block_serialized;
		std::string round_1;
		std::string round_2;
		block_serialized = blockToString(b4);
		round_1 = c.getSHA256(block_serialized);
		round_2 = c.getSHA256(round_1);
		std::cout<<"Hash: 0x"<<round_2<<std::endl;
	} else {
		std::cout<<"Error!"<<std::endl;
	}
}