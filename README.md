# README #


### What is this repository for? ###

This is a C++ implementation of the SHA-256 cryptographic hashing algorithm. 
It also includes a 'toy', proof-of-concept miner program that I wrote for a Bitcoin-like (double SHA-256 PoW) blockchain to get a feel for blockchain and cryptocurrency technologies. It is multithreaded for quad-core computers. 

### How do I get set up? ###

1. Get g++ compiler installed and working
2. Compile using g++
```
#!bash

g++ -O3 miner.cpp -o miner
```

3. Run program

### How well does it work? ###
I modified miner.cpp to spawn four miner threads and I tried compiling the program using the Intel C Compiler. I was able to squeeze out 600 KH/s of double-SHA256 from my MacBook Pro (2.6 GHz 4th-Generation i5 2C/4T). Not practical for real mining by several orders of magnitude, but not bad for a CPU miner running on a small laptop.  

### Resources ###
* [SHA-256 JavaScript Implementation](http://www.movable-type.co.uk/scripts/sha256.html)
* [SHA-256 Pseudocode](https://en.wikipedia.org/wiki/SHA-2#Pseudocode)
* [Circular Right Shift](https://stackoverflow.com/questions/25799215/bitwise-rotation-circular-shift)

### Who do I talk to? ###

Lawrence Fan